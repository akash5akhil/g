#include<iostream>
#include<string.h>
using namespace std;

#define MAX 20

class MST

{
     private:
           struct vertex
        {
            int no;
            char name[MAX];
        };

		vertex node[MAX];

		struct graph
        {
            int v0,v1,cost,flag;
        };

		graph G[MAX];

		int v,e;            //no. of vertices and no.of edges

    public:
           MST();
           void kruskal();
           void Display();
           int findvertex(char city[]);
};

MST::MST()                 //explicit way of writing constructor

{
     int i;               //Initializing graph structure fields with -1
     for(i=0;i<MAX;i++)
     {
        G[i].v0=-1;
        G[i].v1=-1;
        G[i].cost=-1;
        G[i].flag=-1;
     }

}

int MST::findvertex(char city[MAX])     //Given a city name returns its vertex no
{
   int i,vertexno;
   for(i=0;i<v;i++)
   {
    if(strcmp(city,node[i].name)==0)
    {
        vertexno=node[i].no;
        break;
    }
   }
   cout << "\n Vertex No. is:"<< vertexno;
   return vertexno;
}

void MST::Display()   //Displaying v0,v1, cost
{
    int i;
    cout<<"\nv0\tv1\tcost\n";
    for(i=0;i<e;i++)
    {
      cout<<G[i].v0<<"\t"<<G[i].v1<<"\t"<<G[i].cost <<"\n";
    }
}

void MST::kruskal()
{
    int c0,c1,k,wt,parent[MAX], count=0,pos,mincost,i,st[MAX][3],n0,n1;
    char land1[20],land2[20];
    cout<<"\nENTER THE NO OF NODES:";
    cin>>v;
    cout<<"\nENTER "<< v <<" cities:";
    for(i=0;i<v;i++)
    {
        node[i].no=i;
        cin>>node[i].name;
    }

    cout<<"\nposition\tcities\n ";
    for(i=0;i<v;i++)
    {
      cout<<node[i].no<<"\t\t"<<node[i].name<<"\n";
    }

    cout<<"\nENTER THE TOTAL NO OF EDGES:";
    cin>>e;

		for(k=0;k<e;k++)                                   //storing edges(v0 v1) and its cost in graph G
        {
            cout<<"\nENTER THE EDGES AND WEIGTH(city1,city2,wt):";
            cin>>land1>>land2>>wt;
            n0=findvertex(land1);
            n1=findvertex(land2);
            G[k].v0=n0;
            G[k].v1=n1;
            G[k].cost=wt;
        }

     Display();
     cout<<"\n";

    for(i=0;i<v;i++)        //to check all nodes appear once
    {
        parent[i]=i;
    }

    while(count<(v-1))
    {
        mincost=999;
        for(i=0;i<e;i++)     //selecting min wt edge
        {
          if((G[i].cost<mincost)&&(G[i].flag!=1))
          {
              mincost=G[i].cost;
              pos=i;
          }
        }
        G[pos].flag=1;
        n0=G[pos].v0;
        n1=G[pos].v1;
        c0=parent[n0];
        c1=parent[n1];
        if(c0!=c1)
        {
          st[count][0]=n0;
          st[count][1]=n1;
          st[count][2]=G[pos].cost;
          count++;

           for(k=0;k<v;k++)
           {
             if(parent[k]==c1)
                parent[k]=c0;
           }
        }
     }

	cout<<"\nv0\tv1\tcost\n";

    mincost=0;
    for(i=0;i<count;i++)
    {
         mincost=mincost+st[i][2];
         cout<<node[st[i][0]].name<<"\t"<<node[st[i][1]].name<<"\t"<<st[i][2]<<"\n";
    }

	cout <<"\n Minimum cost of given spanning tree is:"<<mincost;
}

int main()

{
        MST k;
        int ch;
        do
        {
           cout<<"\n**MENU**";
           cout<<"\n1.FIND MST";
           cout<<"\n2.EXIT";
           cout<<"\n\nENTER CHOICE : ";
           cin>>ch;

           if(ch == 1)
              k.kruskal();

           if(ch == 2)
              break;

           else
              cout<<"\nINVALID CHOICE !! ";

        }while(ch!=2);

  	cout<<"\nYOU EXITED !!";

        return 0;

}









