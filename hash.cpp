#include<iostream>
#include<stdlib.h>
#include<string.h>
using namespace std;

#define MAX 10

class hasht
{
   struct student
   {
      long long int telephoneno;
      char sname[20];
      int chain;
   };
   student ht[MAX];

  public:
      hasht()   //Constructor :Initializing telephone no. and chain info. with -1 and name as empty string
      {
        for(int i=0;i<MAX;i++)
        {
           ht[i].telephoneno=ht[i].chain=-1;
           strcpy(ht[i].sname,"-");
        }
      }

  void addwithoutrecord(); //add a record with chaining and without replacement
  void addwithrecord(); //add a record with chaining and with replacement
  void display();     // Display records present in hash table
  int computehash( int phoneno); //computes hash key
};

int hasht::computehash( int phoneno)
{
   int  key;
   key=(int)phoneno%10 ;
   return key;
}

void hasht::addwithoutrecord()
{
   int i,k,phoneno;
   char name[MAX],ans;

   do
   {
      cout<<"\n Enter the telephone no:"<<endl;
      cin>>phoneno;
      cout<<"\n Enter the student name:"<<endl;
      cin>>name;

      k=i=computehash(phoneno);

      if(ht[i].telephoneno==-1)
      {
            ht[i].telephoneno=phoneno;
            strcpy(ht[i].sname,name);
      }
      else
      {
         do
         {
             k=(k+1)%10;                            //next empty location
         }while(k!=i && ht[k].telephoneno!=-1);

         if(i==k)
         {
            cout<<"\nhash table is full"<<endl;
            return;
         }
         else
         {
             ht[k].telephoneno=phoneno;
             strcpy(ht[k].sname,name);

              if((computehash(ht[i].telephoneno))==(computehash(ht[k].telephoneno)))  //check for synonym & update chain value
              {
                 while(ht[i].chain!=-1)
                 {
                    i=ht[i].chain;
                 }
                   ht[i].chain=k;
              }
              cout<<"val is added at pos"<<k<<endl;
          }
        }

    display();
    cout<<"do you want to add more ?(y/n)"<<endl;
    cin>>ans;
   }while(ans=='y');
}

void hasht::addwithrecord()
{
   int i,j,k,phoneno;
   char name[MAX],ans;

  do
  {

      cout<<"\n Enter the telephone no:"<<endl;
      cin>>phoneno;
      cout<<"\n Enter the student name:"<<endl;
      cin>>name;

      k=i=computehash(phoneno);

      if(ht[i].telephoneno==-1)
      {
            ht[i].telephoneno=phoneno;
            strcpy(ht[i].sname,name);
      }
      else
      {
         do
         {
             k=(k+1)%10;
         }while(k!=i && ht[k].telephoneno!=-1);

         if(i==k)
         {
            cout<<"\nhash table is full"<<endl;
            return;
         }
         if(computehash(ht[i].telephoneno)==i)  //check if original no
         {
               ht[k].telephoneno=phoneno;
               strcpy(ht[k].sname,name);

               if((computehash(ht[i].telephoneno))==(computehash(ht[k].telephoneno)))  //check for synonym
               {
                    while(ht[i].chain!=-1)
                    {
                        i=ht[i].chain;
                    }
                     ht[i].chain=k;
               }
             cout<<"\n val is added at pos"<<k;
         }
         else                                          //if not original,then replace
         {
             cout<<"\n val "<<ht[i].telephoneno <<" should be replace from "<<i<<" position "<<endl;
             ht[k].telephoneno=ht[i].telephoneno;      //moving non-original no to k(next empty location) position
             strcpy(ht[k].sname,ht[i].sname);
             ht[k].chain=ht[i].chain;

             j=computehash(ht[i].telephoneno);          //update chain value

             while(ht[j].chain!=i)
             {
                 j=ht[j].chain;
             }
              ht[j].chain=k;

              ht[i].telephoneno=phoneno;                //storing original no values
              strcpy(ht[i].sname,name);
              ht[i].chain=-1;
              cout<<"\n value is added at position "<<i<<endl;
          }
       }
    display();
    cout<<"do you want to add more ?(y/n)"<<endl;
    cin>>ans;
    }while(ans=='y');
}

void hasht::display()
 {
   cout<<"position\t\ttelephoneno\t\tname\t\t\tchain"<<endl;
   for(int i=0;i<MAX;i++)
   {
   cout<<i<<"\t\t\t"<<ht[i].telephoneno<<"\t\t"<<ht[i].sname<<"\t\t\t"<<ht[i].chain<<endl;
   }
 }

int main()
{
    hasht htc,htcr;
    int ch;

    do
    {
      cout << "\n1. Chaining Without Replacement \n2. Chaining With Replacement \n3. Exit";
      cout <<"\n Enter your choice:";
      cin >> ch;

      switch(ch)
      {
          case 1: htc.addwithoutrecord(); //chaining without replacement
                  break;
          case 2: htcr.addwithrecord();   // chaining with replacement
                  break;
          case 3: exit(0);
                  break;
          default: cout << "\n Wrong Choice";
      }//end of switch
    }while(ch!=3);
   return 0;
}
