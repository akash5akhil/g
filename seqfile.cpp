#include<iostream>
#include<fstream>
using namespace std;

class Student
{
    int rno;
    char name[50];
    char div;
    char address[100];
public:
    void setData()
    {
        cout<<"\nENTER ROLL NO. : ";
        cin>>rno;

        cout<<"\n\nENTER YOUR NAME : ";
        cin>>name;

        cout<<"\n\nENTER DIVISION : ";
        cin>>div;

        cout<<"\n\nENTER ADDRESS : ";
        cin>>address;

    }

    void showData()
    {
        cout<<"\nROLL NO. : "<<rno;
        cout<<"\nNAME     : "<<name;
        cout<<"\nDIVISION : "<<div;
        cout<<"\nADDRESS  : "<<address<<endl;
    }

    int retrno()
    {
        return rno;
    }
};

//FUNCTION TO WRITE BINARY FILE
void write_record()
{
    ofstream outFile;              //create file and write info to file
    outFile.open("student.txt", ios::binary | ios::app);

    Student obj;
    obj.setData();

    outFile.write((char*)&obj, sizeof(obj));

    outFile.close();
}

//FUNCTION TO READ FROM BINARY FILE AND SHOW ON CONSOLE
void display()
{
    ifstream inFile;               //read info from file
    inFile.open("student.txt", ios::binary);

    Student obj;

    while(inFile.read((char*)&obj, sizeof(obj)))
    {
        obj.showData();
    }

    inFile.close();
}

//FUNCTION TO SEARCH RECORD
int search(int n)
{
    ifstream inFile;
    inFile.open("student.txt", ios::binary);

    Student obj;

    while(inFile.read((char*)&obj, sizeof(obj)))
    {
        if(obj.retrno() == n)
        {
          obj.showData();
          cout<<"\nRECORD FOUND!!"<<endl;
          return 1;

        }
    }
    inFile.close();
    return 2;
}

//FUNCTION TO DELETE RECORD
void delete_record(int n)
{
    ifstream inFile;
    inFile.open("student.txt", ios::binary);

    ofstream outFile;
    outFile.open("temp.txt", ios::out | ios::binary);

    Student obj;

    while(inFile.read((char*)&obj, sizeof(obj)))
    {
        if(obj.retrno() != n)
        {
            outFile.write((char*)&obj, sizeof(obj));
        }
    }

    inFile.close();
    outFile.close();

    remove("student.txt");
    rename("temp.txt", "student.txt");
}

//FUNCTION TO MODIFY RECORD
void modify_record(int n)
{
    fstream file;
    file.open("student.txt",ios::in | ios::out);

    Student obj;

    while(file.read((char*)&obj, sizeof(obj)))
    {
        if(obj.retrno() == n)
        {
            cout <<"\nENTER THE DETAILS FOR NEW RECORD : \n";
            obj.setData();

            int pos = -1 * sizeof(obj);
            file.seekp(pos, ios::cur);

            file.write((char*)&obj, sizeof(obj));
        }
    }

    file.close();
}

int main()
{
	int n,max,ret,ch;
	max = 0;

	do
	{
		cout<<"\n";
		cout<<"\n1.CREATE FILE\n2.DISPLAY RECORDS\n3.SEARCH RECORD\n4.DELETE RECORD\n5.MODIFY RECORD\n6.EXIT"<<endl;
		cout<<"\nENTER YOUR CHOICE : ";
		cin>>ch;

		switch(ch)
		{
		 case 1: write_record();
		         max++;
			     break;

		 case 2: cout<<"\nLIST OF RECORDS :"<<endl;
		         display();
                 break;

		 case 3: cout<<"\nENTER THE ROLL NO. TO SEARCH : ";
		         cin>>n;
		         cout << "\nSEARCH RESULT : ";
		         ret=search(n);
		         if(ret==2)
		          cout<<"\nRECORD NOT FOUND!!"<<endl;
                 break;


		 case 4: cout<<"\nENTER ROLL NO. TO DELETE RECORD : ";
		         cin>>n;
		         ret=search(n);
		         if(ret==2)
		          cout<<"\nRECORD NOT FOUND!!";
		         else
		         {
		           delete_record(n);
		           cout<<"\nRECORD DELETED!!";
		         }
                 break;

		 case 5: cout<<"\nENTER ROLL NO. TO CHANGE INFORMATION : "<<endl;
                 cin>>n;
                 ret=search(n);
                 if(ret==2)
                  cout<<"\nRECORD NOT FOUND!!";
                 modify_record(n);
                 cout<<endl<<"\nRECORD MODIFIED!!<<endl";
                 break;

		 case 6: cout<<"\nQUIT!";
			     break;

		}

}while(ch<6);

 return 0;
}


