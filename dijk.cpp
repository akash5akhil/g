#include<iostream>
#include<stdlib.h>
using namespace std;

#define INFINITY 999
#define MAX 10

class graph
{
	int G[MAX][MAX],n;

public:
	graph()
{
		n=0;
}

	void readgraph();
	void printgraph();
	void dijk(int startnode);
};

void graph::readgraph()
{
	int i,j;
	cout<<"Enter the no. of vertices:";
	cin>>n;
	cout<<"Enter adjacency matrix:";

	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			cin>>G[i][j];
			if(G[i][j]==0)
			{
				G[i][j]=INFINITY;
			}
		}
	}
}

void graph::printgraph()
{
	int i,j;

	for(i=0;i<n;i++)
	{
		cout<<"\n";
		for(j=0;j<n;j++)
		{
         cout<<" "<<G[i][j];
		}
	}
}

void graph::dijk(int startnode)
{
	int cost[MAX][MAX],distance[MAX],pred[MAX],visited[MAX];
	int count,mindistance,nextnode,i,j;

	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
         cost[i][j]=G[i][j];
		}
	}

	for(i=0;i<n;i++)
	{
		distance[i]=cost[startnode][i];
		pred[i]=startnode;
		visited[i]=0;
	}

	distance[startnode]=0;
	visited[startnode]=1;
	count=1;

	while(count<n-1)
	{
		mindistance=INFINITY;

		for(i=0;i<n;i++)
		{
			if(distance[i]<mindistance && !visited[i])
			{
				mindistance=distance[i];
				nextnode=i;
			}
		}

		visited[nextnode]=1;

		for(i=0;i<n;i++)
		{
			if(!visited[i])
				if(mindistance+cost[nextnode][i]<distance[i])
				{
					distance[i]=mindistance+cost[nextnode][i];
					pred[i]=nextnode;
				}
		}
		count++;
	}

	for(i=0;i<n;i++)
	{
		if(i!=startnode)
		{
			cout<<"\n Distance of node "<< i <<" is "<< distance[i];
			cout<<"\n Shortest Path is:"<< i;
			j=i;
			do
			{
				j=pred[j];
				cout<<"<--"<<j;
			}while(j!=startnode);
		}
	}
}

int main()
{
	graph g;
	int u;
	g.readgraph();
	g.printgraph();
	cout<<"\nEnter the starting node:";
	cin>>u;
	g.dijk(u);
}
